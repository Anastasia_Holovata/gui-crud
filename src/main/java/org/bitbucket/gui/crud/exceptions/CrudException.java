package org.bitbucket.gui.crud.exceptions;

public class CrudException extends RuntimeException{

    public CrudException() {
    }

    public CrudException(String message) {
        super(message);
    }
}
