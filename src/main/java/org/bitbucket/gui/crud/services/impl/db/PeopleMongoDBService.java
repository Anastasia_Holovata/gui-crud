package org.bitbucket.gui.crud.services.impl.db;

import org.bitbucket.gui.crud.entity.Person;
import org.bitbucket.gui.crud.repository.IPeopleRepo;
import org.bitbucket.gui.crud.services.IPeopleService;

import java.util.List;

public class PeopleMongoDBService implements IPeopleService {

    private final IPeopleRepo peopleRepo;

    public PeopleMongoDBService(IPeopleRepo peopleRepo) {
        this.peopleRepo = peopleRepo;
    }

    @Override
    public Person create(Person person) {
        return this.peopleRepo.save(person);
    }

    @Override
    public List<Person> readAll() {
        return this.peopleRepo.findAll();
    }

    @Override
    public void update(long id, Person person) {
        this.peopleRepo.update(id, person);
    }

    @Override
    public void delete(long id) {
        this.peopleRepo.remove(id);
    }
}