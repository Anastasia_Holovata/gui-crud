package org.bitbucket.gui.crud.config;

import org.bitbucket.gui.crud.commands.ChooseDbCommands;
import org.bitbucket.gui.crud.commands.CrudCommands;
import org.bitbucket.gui.crud.commands.PersonDialogCommands;
import org.bitbucket.gui.crud.entity.Person;
import org.bitbucket.gui.crud.models.PeopleTableModel;
import org.bitbucket.gui.crud.services.impl.db.PeoplePostgreSQLService;
import org.bitbucket.gui.crud.utils.ReflectionUtils;
import org.bitbucket.gui.crud.view.*;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.List;

public class ViewConfig {

    private static List<Field> personColumn = ReflectionUtils.fields(Person.class);

    private static PeopleTableModel peopleTableModel =
            new PeopleTableModel(new PeoplePostgreSQLService(RepositoryConfig.peopleRepoPostgreSql()), personColumn)
                    .refresh();
//            new PeopleTableModel(new PeopleCsvService(new FormatCsv(), "people.csv"), personColumn)
//                    .refresh();


    private static PersonDialogCommands personDialogCommands() {
        return new PersonDialogCommands();
    }

    private static ChooseDbCommands chooseDbCommands(PersonDialog personDialog, PeopleTablePanel peopleTablePanel) {
        return new ChooseDbCommands(peopleTableModel, personDialog, peopleTablePanel);
    }

    private static CrudCommands crudCommands(PersonDialog personDialog, PeopleTablePanel peopleTablePanel) {
        return new CrudCommands(peopleTableModel, personDialog, peopleTablePanel);
    }

    private static PeopleChooseDbPanel peopleChooseDbPanel(ChooseDbCommands dbCommands) {
        return new PeopleChooseDbPanel(dbCommands);
    }

    public static PeopleCrudCommandsPanel peopleCrudCommandsPanel(CrudCommands crudCommands) {
        return new PeopleCrudCommandsPanel(crudCommands);
    }

    private static PersonDialog personDialog() {
        return new PersonDialog(personDialogCommands());
    }

    public static PeopleTablePanel peopleTablePanel() {
        return new PeopleTablePanel(peopleTableModel);
    }

    private static JCrudPane jCrudPane(CrudCommands crudCommands, ChooseDbCommands dbCommands) {
        return new JCrudPane(crudCommands, dbCommands);
    }

    public static JFrame peopleFrame() {
        PeopleTablePanel peopleTablePanel = peopleTablePanel();
        CrudCommands crudCommands = crudCommands(personDialog(), peopleTablePanel);
        ChooseDbCommands dbCommands = chooseDbCommands(personDialog(), peopleTablePanel);
        JCrudPane jCrudPane = jCrudPane(crudCommands, dbCommands);
        return new PeopleForm(peopleTablePanel, jCrudPane);
    }
}
