package org.bitbucket.gui.crud.repository.impl;

import org.bitbucket.gui.crud.utils.JDBCConnectionPool;

public class PeopleMySqlRepo extends AbstractJDBCRepo {

    public PeopleMySqlRepo(JDBCConnectionPool pool) {
        super(pool);
    }
}
