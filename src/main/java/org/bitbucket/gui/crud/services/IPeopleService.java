package org.bitbucket.gui.crud.services;

import org.bitbucket.gui.crud.entity.Person;
import org.bitbucket.gui.crud.utils.people.PersonNotNull;

import java.util.List;

public interface IPeopleService {

    Person create(@PersonNotNull Person person);

    List<Person> readAll();

    void update(long id, @PersonNotNull Person person);

    void delete(long id);

    default IPeopleService path(String path) {
        return null;}

    default void clear(){

    }

    default List<Person> getPeople() {
        return null;
    }

}
