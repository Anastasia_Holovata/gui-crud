package org.bitbucket.gui.crud.repository.impl;

import org.bitbucket.gui.crud.utils.JDBCConnectionPool;

public class PeopleH2Repo extends AbstractJDBCRepo {

    public PeopleH2Repo(JDBCConnectionPool pool) {
        super(pool);
    }
}
