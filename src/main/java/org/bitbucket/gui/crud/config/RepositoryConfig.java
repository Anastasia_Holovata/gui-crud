package org.bitbucket.gui.crud.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bitbucket.gui.crud.repository.IPeopleRepo;
import org.bitbucket.gui.crud.repository.impl.*;
import org.bitbucket.gui.crud.utils.JDBCConnectionPool;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import static org.bitbucket.gui.crud.config.ApplicationConfig.*;

public class RepositoryConfig {

    public static IPeopleRepo peopleRepoCassandra() {
        return new PeopleCassandraRepo();
    }

    public static IPeopleRepo peopleRepoGraphDB() {
        return new PeopleGraphDBRepo();
    }

    public static IPeopleRepo peopleRepoH2() {
        return new PeopleH2Repo(new JDBCConnectionPool(
                DB_TIMEOUT,
                "org.h2.Driver",
                "jdbc:h2:mem:crud;MODE=MySQL;INIT=RUNSCRIPT FROM 'src/main/resources/init-h2.sql'",
                DB_H2_USER,
                DB_H2_PASSWORD));
    }

    public static IPeopleRepo peopleRepoMongoDB() {
        return new PeopleMongoDBRepo(createMongoClient());
    }

    public static IPeopleRepo peopleRepoMySql() {
        return new PeopleMySqlRepo(new JDBCConnectionPool(
                DB_TIMEOUT,
                "com.mysql.cj.jdbc.Driver",
                getUrl("mysql", DB_HOST, DB_MYSQL_PORT, DB_NAME),
                DB_MYSQL_USER,
                DB_MYSQL_PASSWORD));
    }

    public static IPeopleRepo peopleRepoPostgreSql() {
        return new PeoplePostgreSQLRepo(new JDBCConnectionPool(
                DB_TIMEOUT,
                "org.postgresql.Driver",
                getUrl("postgresql", DB_HOST, DB_POSTGRES_PORT, DB_NAME),
                DB_POSTGRES_USER,
                DB_POSTGRES_PASSWORD));
    }

    public static IPeopleRepo peopleRepoRedis() {
        return new PeopleRedisRepo(createRedisClient());
    }

    private static String getUrl(String dbTypeName, String host, int port, String dbName) {
        return "jdbc:" + dbTypeName + "://" + host + ":" + port + "/" + dbName;
    }

    private static RedissonClient createRedisClient() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + DB_HOST + ":" + DB_REDIS_PORT);

        return Redisson.create(config);
    }

    private static MongoClient createMongoClient() {
        return MongoClients.create("mongodb://" + DB_HOST + ":" + DB_MONGO_PORT + "/" + DB_NAME);
    }
}
