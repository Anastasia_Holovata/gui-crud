package org.bitbucket.gui.crud;

import org.bitbucket.gui.crud.config.ViewConfig;

import java.awt.*;

public class Application {
    public static void main(String[] args) {
        ViewConfig.peopleFrame().getContentPane();
    }
}
