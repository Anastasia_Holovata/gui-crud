package org.bitbucket.gui.crud.view;

import org.bitbucket.gui.crud.commands.CrudCommands;
import org.bitbucket.gui.crud.config.type.FileType;

import javax.swing.*;

public class PeopleCrudCommandsPanel extends JPanel {

    private FileType fileType;

    public PeopleCrudCommandsPanel(FileType fileType) {
        this.fileType = fileType;
    }

    private static String[] repositories = new String[]{
            "Mock", "lock", ".json", ".bin",".csv",".xml", ".yml",
    };

    public PeopleCrudCommandsPanel(CrudCommands commands) {
        setLayout(null);

        setBounds(5, 8, 315, 845);

        JButton createButton = new JButton("Create");
        JButton readButton = new JButton("Read");
        JButton updateButton = new JButton("Update");
        JButton removeButton = new JButton("Remove");
        JComboBox<String> cmbBxDb = new JComboBox<>();


        createButton.setBounds(25, 35, 265, 85);
        readButton.setBounds(25, 135, 265, 85);
        updateButton.setBounds(25, 235, 265, 85);
        removeButton.setBounds(25, 335, 265, 85);
        cmbBxDb.setBounds(25, 435, 265, 40);

        createButton.addActionListener(commands.actionCreate());
        readButton.addActionListener(commands.actionRead());
        updateButton.addActionListener(commands.actionUpdate());
        removeButton.addActionListener(commands.actionDelete());
        cmbBxDb.addActionListener(commands.chooseLocalFormat());

        add(createButton);
        add(readButton);
        add(updateButton);
        add(removeButton);
        add(cmbBxDb);

        setVisible(Boolean.TRUE);
    }
}
