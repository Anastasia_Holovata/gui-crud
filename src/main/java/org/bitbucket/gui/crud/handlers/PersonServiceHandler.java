package org.bitbucket.gui.crud.handlers;

import org.bitbucket.gui.crud.services.IPeopleService;
import org.bitbucket.gui.crud.utils.people.PersonNotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Objects;

public class PersonServiceHandler implements InvocationHandler {

    private static final Logger log = LoggerFactory.getLogger(PersonServiceHandler.class);

    private final IPeopleService peopleService;

    public PersonServiceHandler(IPeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.info("Before call to method: {}, with args: {}", method.getName(), args);
        Parameter[] params = method.getParameters();
        for (int i = 0; i < params.length; i++) {
            if(params[i].isAnnotationPresent(PersonNotNull.class)){
                if(Objects.isNull(args[i])){
                    throw new IllegalArgumentException("Person can not be null!");
                }
            }
        }
        Object result = method.invoke(peopleService, args);
        log.info("After call to method: {}", result);
        return result;
    }
}
