package org.bitbucket.gui.crud.view;

import org.bitbucket.gui.crud.commands.ChooseDbCommands;
import org.bitbucket.gui.crud.commands.CrudCommands;

import javax.swing.*;

public class JCrudPane extends JTabbedPane {

    public JCrudPane(CrudCommands commands, ChooseDbCommands dbCommands) {

        this.setBounds(5, 8, 315, 845);

        JPanel crudPanel = new PeopleCrudCommandsPanel(commands);
        crudPanel.setVisible(Boolean.TRUE);
        JPanel dbPanel = new PeopleChooseDbPanel(dbCommands);
        dbPanel.setVisible(Boolean.TRUE);

        add("CRUD",crudPanel);
        add("DB",dbPanel);
        setVisible(Boolean.TRUE);
    }

}
