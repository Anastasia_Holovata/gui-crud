import org.bitbucket.gui.crud.config.ServicesConfig;
import org.bitbucket.gui.crud.services.IPeopleService;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServicesConfigTest {

    @Test(expected = IllegalArgumentException.class)
    public void personNotNull() {
        IPeopleService instance = ServicesConfig.servicePeoplePostgreSQL();
        instance.create(null);
    }

}