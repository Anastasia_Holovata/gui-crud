package org.bitbucket.gui.crud.formats;

import org.bitbucket.gui.crud.entity.Person;

import java.util.List;

public interface IBinFormat {

    List<Person> loadObject(String fileName);

    void saveObject(List<Person> people, String fileName);
}
