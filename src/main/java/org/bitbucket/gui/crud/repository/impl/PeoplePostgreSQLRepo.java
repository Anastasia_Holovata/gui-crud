package org.bitbucket.gui.crud.repository.impl;

import org.bitbucket.gui.crud.utils.JDBCConnectionPool;

public class PeoplePostgreSQLRepo extends AbstractJDBCRepo {

    public PeoplePostgreSQLRepo(JDBCConnectionPool pool) {
        super(pool);
    }
}
