package org.bitbucket.gui.crud.repository.impl;

import org.bitbucket.gui.crud.entity.Person;
import org.bitbucket.gui.crud.repository.IPeopleRepo;

import java.util.List;

public class PeopleCassandraRepo implements IPeopleRepo {

    @Override
    public Person save(Person p) {
        return null;
    }

    @Override
    public List<Person> findAll() {
        return null;
    }

    @Override
    public void update(long id, Person p) {

    }

    @Override
    public void remove(long id) {

    }
}
