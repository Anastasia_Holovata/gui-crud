package org.bitbucket.gui.crud.commands;

import org.bitbucket.gui.crud.config.DataBaseConfig;
import org.bitbucket.gui.crud.config.type.DataBaseType;
import org.bitbucket.gui.crud.entity.Person;
import org.bitbucket.gui.crud.models.PeopleTableModel;
import org.bitbucket.gui.crud.view.PeopleTablePanel;
import org.bitbucket.gui.crud.view.PersonDialog;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ChooseDbCommands {

    private final PeopleTableModel peopleTableModel;
    private final PersonDialog personDialog;
    private final PeopleTablePanel peopleTablePanel;

    public ChooseDbCommands(PeopleTableModel peopleTableModel, PersonDialog personDialog, PeopleTablePanel peopleTablePanel) {
        this.peopleTableModel = peopleTableModel;
        this.personDialog = personDialog;
        this.peopleTablePanel = peopleTablePanel;
    }

    public ActionListener chooseDataBase() {
        return e -> {
            JComboBox<String> dataBaseNameSource = (JComboBox) e.getSource();
            String dataBaseName = (String) dataBaseNameSource.getSelectedItem();
            this.peopleTableModel.setPeopleService(DataBaseConfig.newInstance(DataBaseType.from(dataBaseName)));
            read();
        };
    }

    public ActionListener actionCreate() {
        return e -> {
            this.personDialog.setVisible(Boolean.TRUE);
            if (this.personDialog.isDialog()) {
                Person person = this.personDialog.getPerson();
                this.peopleTableModel.create(person);
                this.personDialog.clear();
                read();
            }
        };
    }

    public ActionListener actionRead() {
        return e -> {
            read();
        };
    }

    public ActionListener actionUpdate() {
        return e -> {
            this.personDialog.setVisibleId(false);
            this.personDialog.setVisible(Boolean.TRUE);
            if (this.personDialog.isDialog()) {
                Person person = this.personDialog.getPerson();
                long selectedPeopleId = getSelectedPeopleId();
                person.setId(selectedPeopleId);
                this.peopleTableModel.update(selectedPeopleId, person);
                this.personDialog.clear();
                read();
            }
            this.personDialog.setVisibleId(true);
        };
    }

    public ActionListener actionDelete() {
        return e -> {
            this.peopleTableModel.delete(getSelectedPeopleId());
            read();
        };
    }

    private long getSelectedPeopleId() {
        JTable peopleTable = peopleTablePanel.getPeopleTable();
        int selectedRow = peopleTable.getSelectedRow();
        return (long) peopleTable.getValueAt(selectedRow, 0);
    }

    private void read() {
        this.peopleTableModel.readAll();
        this.peopleTablePanel.getPeopleTable().revalidate();
        this.peopleTablePanel.repaint();
    }
}
