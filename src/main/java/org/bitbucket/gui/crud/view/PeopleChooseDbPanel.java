package org.bitbucket.gui.crud.view;

import org.bitbucket.gui.crud.commands.ChooseDbCommands;
import org.bitbucket.gui.crud.commands.CrudCommands;

import javax.swing.*;

public class PeopleChooseDbPanel extends JPanel {

    private static String[] repositories = new String[]{
            "Cassandra", "GraphDB", "H2", "MongoDB", "MySql", "PostgreSql", "Redis",
    };

    public PeopleChooseDbPanel(ChooseDbCommands dbCommands) {
        setLayout(null);

        setBounds(5, 8, 315, 845);

        JButton createButton = new JButton("Create");
        JButton readButton = new JButton("Read");
        JButton updateButton = new JButton("Update");
        JButton removeButton = new JButton("Remove");

        JComboBox<String> cmbBxDb = new JComboBox<>(repositories);

        createButton.setBounds(25, 35, 265, 85);
        readButton.setBounds(25, 135, 265, 85);
        updateButton.setBounds(25, 235, 265, 85);
        removeButton.setBounds(25, 335, 265, 85);
        cmbBxDb.setBounds(25, 435, 265, 40);

        cmbBxDb.addActionListener(dbCommands.chooseDataBase());
        createButton.addActionListener(dbCommands.actionCreate());
        readButton.addActionListener(dbCommands.actionRead());
        updateButton.addActionListener(dbCommands.actionUpdate());
        removeButton.addActionListener(dbCommands.actionDelete());

        add(createButton);
        add(readButton);
        add(updateButton);
        add(removeButton);
        add(cmbBxDb);

        setVisible(Boolean.TRUE);
    }

}
